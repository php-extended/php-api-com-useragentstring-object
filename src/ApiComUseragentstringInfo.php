<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring;

/**
 * ApiComUseragentstringInfo class file.
 * 
 * This is a simple implementation of the ApiComUseragentstringInfoInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiComUseragentstringInfo implements ApiComUseragentstringInfoInterface
{
	
	/**
	 * The family of the user agent.
	 * 
	 * @var string
	 */
	protected string $_family;
	
	/**
	 * The browser of the user agent.
	 * 
	 * @var string
	 */
	protected string $_browser;
	
	/**
	 * The user agent value.
	 * 
	 * @var string
	 */
	protected string $_userAgent;
	
	/**
	 * The id of the user agent in the useragentstring database.
	 * 
	 * @var int
	 */
	protected int $_identifier;
	
	/**
	 * Constructor for ApiComUseragentstringInfo with private members.
	 * 
	 * @param string $family
	 * @param string $browser
	 * @param string $userAgent
	 * @param int $identifier
	 */
	public function __construct(string $family, string $browser, string $userAgent, int $identifier)
	{
		$this->setFamily($family);
		$this->setBrowser($browser);
		$this->setUserAgent($userAgent);
		$this->setIdentifier($identifier);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the family of the user agent.
	 * 
	 * @param string $family
	 * @return ApiComUseragentstringInfoInterface
	 */
	public function setFamily(string $family) : ApiComUseragentstringInfoInterface
	{
		$this->_family = $family;
		
		return $this;
	}
	
	/**
	 * Gets the family of the user agent.
	 * 
	 * @return string
	 */
	public function getFamily() : string
	{
		return $this->_family;
	}
	
	/**
	 * Sets the browser of the user agent.
	 * 
	 * @param string $browser
	 * @return ApiComUseragentstringInfoInterface
	 */
	public function setBrowser(string $browser) : ApiComUseragentstringInfoInterface
	{
		$this->_browser = $browser;
		
		return $this;
	}
	
	/**
	 * Gets the browser of the user agent.
	 * 
	 * @return string
	 */
	public function getBrowser() : string
	{
		return $this->_browser;
	}
	
	/**
	 * Sets the user agent value.
	 * 
	 * @param string $userAgent
	 * @return ApiComUseragentstringInfoInterface
	 */
	public function setUserAgent(string $userAgent) : ApiComUseragentstringInfoInterface
	{
		$this->_userAgent = $userAgent;
		
		return $this;
	}
	
	/**
	 * Gets the user agent value.
	 * 
	 * @return string
	 */
	public function getUserAgent() : string
	{
		return $this->_userAgent;
	}
	
	/**
	 * Sets the id of the user agent in the useragentstring database.
	 * 
	 * @param int $identifier
	 * @return ApiComUseragentstringInfoInterface
	 */
	public function setIdentifier(int $identifier) : ApiComUseragentstringInfoInterface
	{
		$this->_identifier = $identifier;
		
		return $this;
	}
	
	/**
	 * Gets the id of the user agent in the useragentstring database.
	 * 
	 * @return int
	 */
	public function getIdentifier() : int
	{
		return $this->_identifier;
	}
	
}
