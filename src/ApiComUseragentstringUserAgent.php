<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring;

use Psr\Http\Message\UriInterface;

/**
 * ApiComUseragentstringUserAgent class file.
 * 
 * This is a simple implementation of the
 * ApiComUseragentstringUserAgentInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiComUseragentstringUserAgent implements ApiComUseragentstringUserAgentInterface
{
	
	/**
	 * The user agent type.
	 * 
	 * @var ?string
	 */
	protected ?string $_agentType = null;
	
	/**
	 * The user agent name.
	 * 
	 * @var ?string
	 */
	protected ?string $_agentName = null;
	
	/**
	 * The user agent version number.
	 * 
	 * @var ?string
	 */
	protected ?string $_agentVersion = null;
	
	/**
	 * The operating system type.
	 * 
	 * @var ?string
	 */
	protected ?string $_osType = null;
	
	/**
	 * The operating system name.
	 * 
	 * @var ?string
	 */
	protected ?string $_osName = null;
	
	/**
	 * The operating system version name.
	 * 
	 * @var ?string
	 */
	protected ?string $_osVersionName = null;
	
	/**
	 * The operating system version number.
	 * 
	 * @var ?string
	 */
	protected ?string $_osVersionNumber = null;
	
	/**
	 * The operating system producer name.
	 * 
	 * @var ?string
	 */
	protected ?string $_osProducer = null;
	
	/**
	 * The operating system producer url.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_osProducerUrl = null;
	
	/**
	 * The linux distribution.
	 * 
	 * @var ?string
	 */
	protected ?string $_linuxDistribution = null;
	
	/**
	 * The user agent language.
	 * 
	 * @var ?string
	 */
	protected ?string $_agentLanguage = null;
	
	/**
	 * The user agent language tag.
	 * 
	 * @var ?string
	 */
	protected ?string $_agentLanguageTag = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the user agent type.
	 * 
	 * @param ?string $agentType
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setAgentType(?string $agentType) : ApiComUseragentstringUserAgentInterface
	{
		$this->_agentType = $agentType;
		
		return $this;
	}
	
	/**
	 * Gets the user agent type.
	 * 
	 * @return ?string
	 */
	public function getAgentType() : ?string
	{
		return $this->_agentType;
	}
	
	/**
	 * Sets the user agent name.
	 * 
	 * @param ?string $agentName
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setAgentName(?string $agentName) : ApiComUseragentstringUserAgentInterface
	{
		$this->_agentName = $agentName;
		
		return $this;
	}
	
	/**
	 * Gets the user agent name.
	 * 
	 * @return ?string
	 */
	public function getAgentName() : ?string
	{
		return $this->_agentName;
	}
	
	/**
	 * Sets the user agent version number.
	 * 
	 * @param ?string $agentVersion
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setAgentVersion(?string $agentVersion) : ApiComUseragentstringUserAgentInterface
	{
		$this->_agentVersion = $agentVersion;
		
		return $this;
	}
	
	/**
	 * Gets the user agent version number.
	 * 
	 * @return ?string
	 */
	public function getAgentVersion() : ?string
	{
		return $this->_agentVersion;
	}
	
	/**
	 * Sets the operating system type.
	 * 
	 * @param ?string $osType
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setOsType(?string $osType) : ApiComUseragentstringUserAgentInterface
	{
		$this->_osType = $osType;
		
		return $this;
	}
	
	/**
	 * Gets the operating system type.
	 * 
	 * @return ?string
	 */
	public function getOsType() : ?string
	{
		return $this->_osType;
	}
	
	/**
	 * Sets the operating system name.
	 * 
	 * @param ?string $osName
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setOsName(?string $osName) : ApiComUseragentstringUserAgentInterface
	{
		$this->_osName = $osName;
		
		return $this;
	}
	
	/**
	 * Gets the operating system name.
	 * 
	 * @return ?string
	 */
	public function getOsName() : ?string
	{
		return $this->_osName;
	}
	
	/**
	 * Sets the operating system version name.
	 * 
	 * @param ?string $osVersionName
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setOsVersionName(?string $osVersionName) : ApiComUseragentstringUserAgentInterface
	{
		$this->_osVersionName = $osVersionName;
		
		return $this;
	}
	
	/**
	 * Gets the operating system version name.
	 * 
	 * @return ?string
	 */
	public function getOsVersionName() : ?string
	{
		return $this->_osVersionName;
	}
	
	/**
	 * Sets the operating system version number.
	 * 
	 * @param ?string $osVersionNumber
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setOsVersionNumber(?string $osVersionNumber) : ApiComUseragentstringUserAgentInterface
	{
		$this->_osVersionNumber = $osVersionNumber;
		
		return $this;
	}
	
	/**
	 * Gets the operating system version number.
	 * 
	 * @return ?string
	 */
	public function getOsVersionNumber() : ?string
	{
		return $this->_osVersionNumber;
	}
	
	/**
	 * Sets the operating system producer name.
	 * 
	 * @param ?string $osProducer
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setOsProducer(?string $osProducer) : ApiComUseragentstringUserAgentInterface
	{
		$this->_osProducer = $osProducer;
		
		return $this;
	}
	
	/**
	 * Gets the operating system producer name.
	 * 
	 * @return ?string
	 */
	public function getOsProducer() : ?string
	{
		return $this->_osProducer;
	}
	
	/**
	 * Sets the operating system producer url.
	 * 
	 * @param ?UriInterface $osProducerUrl
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setOsProducerUrl(?UriInterface $osProducerUrl) : ApiComUseragentstringUserAgentInterface
	{
		$this->_osProducerUrl = $osProducerUrl;
		
		return $this;
	}
	
	/**
	 * Gets the operating system producer url.
	 * 
	 * @return ?UriInterface
	 */
	public function getOsProducerUrl() : ?UriInterface
	{
		return $this->_osProducerUrl;
	}
	
	/**
	 * Sets the linux distribution.
	 * 
	 * @param ?string $linuxDistribution
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setLinuxDistribution(?string $linuxDistribution) : ApiComUseragentstringUserAgentInterface
	{
		$this->_linuxDistribution = $linuxDistribution;
		
		return $this;
	}
	
	/**
	 * Gets the linux distribution.
	 * 
	 * @return ?string
	 */
	public function getLinuxDistribution() : ?string
	{
		return $this->_linuxDistribution;
	}
	
	/**
	 * Sets the user agent language.
	 * 
	 * @param ?string $agentLanguage
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setAgentLanguage(?string $agentLanguage) : ApiComUseragentstringUserAgentInterface
	{
		$this->_agentLanguage = $agentLanguage;
		
		return $this;
	}
	
	/**
	 * Gets the user agent language.
	 * 
	 * @return ?string
	 */
	public function getAgentLanguage() : ?string
	{
		return $this->_agentLanguage;
	}
	
	/**
	 * Sets the user agent language tag.
	 * 
	 * @param ?string $agentLanguageTag
	 * @return ApiComUseragentstringUserAgentInterface
	 */
	public function setAgentLanguageTag(?string $agentLanguageTag) : ApiComUseragentstringUserAgentInterface
	{
		$this->_agentLanguageTag = $agentLanguageTag;
		
		return $this;
	}
	
	/**
	 * Gets the user agent language tag.
	 * 
	 * @return ?string
	 */
	public function getAgentLanguageTag() : ?string
	{
		return $this->_agentLanguageTag;
	}
	
}
