<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring;

use ArrayIterator;
use DateTimeImmutable;
use Iterator;
use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\Html\HtmlCollectionNodeInterface;
use PhpExtended\Html\HtmlParser;
use PhpExtended\Html\HtmlParserInterface;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;

/**
 * ApiComUseragentstringEndpoint class file.
 * 
 * This class acts as an endpoint for the useragentstring.com API.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiComUseragentstringEndpoint implements ApiComUseragentstringEndpointInterface
{
	
	/**
	 * The default hostname of the endpoint.
	 * 
	 * @var string
	 */
	public const HOST = 'http://useragentstring.com';
	
	/**
	 * The http client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 * 
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 * 
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The html parser.
	 * 
	 * @var HtmlParserInterface
	 */
	protected HtmlParserInterface $_htmlParser;
	
	/**
	 * The reifier.
	 * 
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * Builds a new ApiComUseragentstringEndpoint with its inner endpoints.
	 * 
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?HtmlParserInterface $htmlParser
	 * @param ?ReifierInterface $reifier
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?HtmlParserInterface $htmlParser = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_htmlParser = $htmlParser ?? new HtmlParser();
		$this->_reifier = $reifier ?? new Reifier();
		
		$configuration = $this->_reifier->getConfiguration();
		// not all of them are needed, some can be resolved automatically
		// by the reifier using the snake case to camel case convention
// 		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'agentType', 'agent_type');
// 		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'agentName', 'agent_name');
// 		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'agentVersion', 'agent_version');
// 		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'osType', 'os_type');
// 		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'osName', 'os_name');
		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'osVersionName', 'os_versionName');
		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'osVersionNumber', 'os_versionNumber');
// 		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'osProducer', 'os_producer');
		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'osProducerUrl', 'os_producerURL');
		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'linuxDistribution', 'linux_distibution');
// 		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'agentLanguage', 'agent_language');
		$configuration->addFieldNameAlias(ApiComUseragentstringUserAgent::class, 'agentLanguageTag', 'agent_languageTag');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiComUseragentstring\ApiComUseragentstringEndpointInterface::getUserAgent()
	 */
	public function getUserAgent(string $userAgentString) : ApiComUseragentstringUserAgentInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'/?uas='.\rawurlencode($userAgentString).'&getJSON=all');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComUseragentstringUserAgent::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiComUseragentstring\ApiComUseragentstringEndpointInterface::getUserAgentInfoIterator()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function getUserAgentInfoIterator() : Iterator
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'/pages/useragentstring.php?name=All');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$dom = $this->_htmlParser->parse($response->getBody()->__toString());
		
		$knownIds = [];
		$elements = [];
		$currentFamily = null;
		$currentBrowser = null;
		
		$list = $dom->findNodeCss('div#liste');
		if(null === $list || !($list instanceof HtmlCollectionNodeInterface))
		{
			$message = 'Failed to find node "{css}" at url {url}';
			$context = ['{css}' => 'div#liste', '{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		foreach($list->getChildren() as $node)
		{
			switch($node->getName())
			{
				case 'h3':
					$currentFamily = \trim($node->getText());
					break;
					
				case 'h4':
					$currentBrowser = \trim($node->getText());
					break;
					
				case 'ul':
					foreach($node->findAllNodesCss('a') as $anchorNode)
					{
						$attribute = $anchorNode->getAttribute('href');
						if(null === $attribute)
						{
							continue;
						}
						$href = $attribute->getValue();
						
						// not a real link, only a link to more links
						if(\preg_match('#^/_uas_#', $href))
						{
							continue;
						}
						
						$uag = \trim($anchorNode->getText());
						
						if(false !== \mb_strpos($href, '/pages/useragentstring.php') && false !== \mb_strpos($uag, 'More') && false !== \mb_strpos($uag, 'user agents strings'))
						{
							// {{{ Inner request
							$uri2 = $this->_uriFactory->createUri(self::HOST.'/'.\trim($href, '/'));
							$request2 = $this->_requestFactory->createRequest('GET', $uri2);
							$response2 = $this->_httpClient->sendRequest($request2);
							$dom2 = $this->_htmlParser->parse($response2->getBody()->__toString());
							
							$list2 = $dom2->findNodeCss('div#liste ul a');
							if(null === $list2 || !($list2 instanceof HtmlCollectionNodeInterface))
							{
								$message = 'Failed to find node "{css}" at url {url}';
								$context = ['{css}' => 'div#liste ul a', '{url}' => $uri->__toString()];
								
								throw new RuntimeException(\strtr($message, $context));
							}
							
							foreach($list2->getChildren() as $anchorNode2)
							{
								$attribute = $anchorNode2->getAttribute('href');
								if(null === $attribute)
								{
									continue;
								}
								$href2 = $attribute->getValue();
								
								// not a real link, only a link to more links
								if(\preg_match('#^/_uas_#', $href2))
								{
									continue;
								}
								
								$ua2 = \trim($anchorNode2->getText());
								
								$matches = [];
								$res = \preg_match('#id_(\\d+)#', $href2, $matches);
								if(false === $res || !isset($matches[1]))
								{
									$message = 'Failed to find id in href tag {tag} for browser "{family}" "{name}" "{ua}" at url {url}';
									$context = ['{tag}' => $href2, '{family}' => $currentFamily, '{name}' => $currentBrowser, '{ua}' => $uag, '{url}' => $uri2->__toString()];
									
									throw new RuntimeException(\strtr($message, $context));
								}
								
								$id = $matches[1];
								
								// already listed, skip
								if(isset($knownIds[$id]))
								{
									continue;
								}
								
								if(null === $currentFamily || '' === $currentFamily)
								{
									$message = 'Failed to get current family for user agent "{ua}" at url {url}';
									$context = ['{ua}' => $ua2, '{url}' => $uri2->__toString()];
									
									throw new RuntimeException(\strtr($message, $context));
								}
								
								if(null === $currentBrowser || '' === $currentBrowser)
								{
									$message = 'Failed to get current browser for user agent "{ua}" at url {url}';
									$context = ['{ua}' => $ua2, '{url}' => $uri2->__toString()];
									
									throw new RuntimeException(\strtr($message, $context));
								}
								
								$elements[] = new ApiComUseragentstringInfo($currentFamily, $currentBrowser, $ua2, (int) $id);
								$knownIds[$id] = 1;
							}
							
							// }}} End Inner request
							
							continue;
						}
						
						// regular user agent string
						$matches = [];
						$res = \preg_match('#id_(\\d+)#', $href, $matches);
						if(false === $res || !isset($matches[1]))
						{
							$message = 'Failed to find id in href tag {tag} for browser "{family}" "{name}" at url {url}';
							$context = ['{tag}' => $href, '{family}' => $currentFamily, '{name}' => $currentBrowser, '{url}' => $uri->__toString()];
							
							throw new RuntimeException(\strtr($message, $context));
						}
						
						$id = $matches[1];
						
						// already listed, skip
						if(isset($knownIds[$id]))
						{
							continue;
						}
						
						if(null === $currentFamily || '' === $currentFamily)
						{
							$message = 'Failed to get current family for user agent "{ua}" at url {url}';
							$context = ['{ua}' => $uag, '{url}' => $uri->__toString()];
							
							throw new RuntimeException(\strtr($message, $context));
						}
						
						if(null === $currentBrowser || '' === $currentBrowser)
						{
							$message = 'Failed to get current browser for user agent "{ua}" at url {url}';
							$context = ['{ua}' => $uag, '{url}' => $uri->__toString()];
							
							throw new RuntimeException(\strtr($message, $context));
						}
						
						$elements[] = new ApiComUseragentstringInfo($currentFamily, $currentBrowser, $uag, (int) $id);
						$knownIds[$id] = 1;
					}
					break;
					
				default:
					// ignore
			}
		}
		
		return new ArrayIterator($elements);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiComUseragentstring\ApiComUseragentstringEndpointInterface::getUserAgentData()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function getUserAgentData(ApiComUseragentstringInfoInterface $info) : ApiComUseragentstringDataInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'/index.php?id='.\urlencode((string) $info->getIdentifier()));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$dom = $this->_htmlParser->parse($response->getBody()->__toString());
		
		$parts = [];
		$description = null;
		$firstVisit = null;
		$lastVisit = null;
		
		$textarea = $dom->findNodeCss('textarea#uas_textfeld');
		if(null === $textarea)
		{
			$message = 'Failed to find node "{css}" at url {url}';
			$context = ['{css}' => 'textarea#uas_textfeld', '{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$uag = \trim($textarea->getText());
		if(empty($uag))
		{
			$message = 'The given id does not correspond to a valid user agent : {id} at {url}';
			$context = ['{id}' => $info->getIdentifier(), '{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$table = $dom->findNodeCss('table#dieTabelle');
		if(null === $table || !($table instanceof HtmlCollectionNodeInterface))
		{
			$message = 'Failed to find collection node "{css}" at url {url}';
			$context = ['{css}' => 'table#dieTabelle', '{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		foreach($table as $tableRow)
		{
			/** @var \PhpExtended\Html\HtmlAbstractNodeInterface $tableRow */
			$keyCell = $tableRow->findNodeCss('td.word');
			if(null === $keyCell)
			{
				continue;
			}
			
			$valCell = $tableRow->findNodeCss('td.erklaerung');
			if(null === $valCell)
			{
				continue;
			}
			
			$keyVal = \trim($keyCell->getText());
			$valVal = \trim($valCell->getText());
			
			switch($keyVal)
			{
				case 'Description:':
					$description = $valVal;
					break;
					
				case 'First visit:':
					$firstVisit = DateTimeImmutable::createFromFormat('Y.m.d H:i', $valVal);
					if(empty($firstVisit))
					{
						$message = 'Failed to parse first visit for user agent {id}, {ua} at url {url}';
						$context = ['{id}' => $info->getIdentifier(), '{ua}' => $uag, '{url}' => $uri->__toString()];
						
						throw new RuntimeException(\strtr($message, $context));
					}
					break;
						
				case 'Last visit:':
					$lastVisit = DateTimeImmutable::createFromFormat('Y.m.d H:i', $valVal);
					if(empty($lastVisit))
					{
						$message = 'Failed to parse last visit for user agent {id} {ua} at url {url}';
						$context = ['{id}' => $info->getIdentifier(), '{ua}' => $uag, '{url}' => $uri->__toString()];
						
						throw new RuntimeException(\strtr($message, $context));
					}
					break;
						
				default:
					$parts[$keyVal] = $valVal;
					break;
			}
		}
		
		$data = new ApiComUseragentstringData($info->getIdentifier(), $uag);
		$data->setParts($parts);
		$data->setDescription($description);
		$data->setFirstVisit($firstVisit);
		$data->setLastVisit($lastVisit);
		
		return $data;
	}
	
}
