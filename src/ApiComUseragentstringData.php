<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring;

use DateTimeInterface;

/**
 * ApiComUseragentstringData class file.
 * 
 * This is a simple implementation of the ApiComUseragentstringDataInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiComUseragentstringData implements ApiComUseragentstringDataInterface
{
	
	/**
	 * The id of the user agent string.
	 * 
	 * @var int
	 */
	protected int $_identifier;
	
	/**
	 * The user agent string value.
	 * 
	 * @var string
	 */
	protected string $_userAgent;
	
	/**
	 * The parts of the user agent string explained individually.
	 * 
	 * @var array<string, string>
	 */
	protected array $_parts = [];
	
	/**
	 * The global description of the user agent.
	 * 
	 * @var ?string
	 */
	protected ?string $_description = null;
	
	/**
	 * The date when this user agent was seen for the first time.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_firstVisit = null;
	
	/**
	 * The date when this user agent was seen for the last time.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_lastVisit = null;
	
	/**
	 * Constructor for ApiComUseragentstringData with private members.
	 * 
	 * @param int $identifier
	 * @param string $userAgent
	 */
	public function __construct(int $identifier, string $userAgent)
	{
		$this->setIdentifier($identifier);
		$this->setUserAgent($userAgent);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the user agent string.
	 * 
	 * @param int $identifier
	 * @return ApiComUseragentstringDataInterface
	 */
	public function setIdentifier(int $identifier) : ApiComUseragentstringDataInterface
	{
		$this->_identifier = $identifier;
		
		return $this;
	}
	
	/**
	 * Gets the id of the user agent string.
	 * 
	 * @return int
	 */
	public function getIdentifier() : int
	{
		return $this->_identifier;
	}
	
	/**
	 * Sets the user agent string value.
	 * 
	 * @param string $userAgent
	 * @return ApiComUseragentstringDataInterface
	 */
	public function setUserAgent(string $userAgent) : ApiComUseragentstringDataInterface
	{
		$this->_userAgent = $userAgent;
		
		return $this;
	}
	
	/**
	 * Gets the user agent string value.
	 * 
	 * @return string
	 */
	public function getUserAgent() : string
	{
		return $this->_userAgent;
	}
	
	/**
	 * Sets the parts of the user agent string explained individually.
	 * 
	 * @param array<string, string> $parts
	 * @return ApiComUseragentstringDataInterface
	 */
	public function setParts(array $parts) : ApiComUseragentstringDataInterface
	{
		$this->_parts = $parts;
		
		return $this;
	}
	
	/**
	 * Gets the parts of the user agent string explained individually.
	 * 
	 * @return array<string, string>
	 */
	public function getParts() : array
	{
		return $this->_parts;
	}
	
	/**
	 * Sets the global description of the user agent.
	 * 
	 * @param ?string $description
	 * @return ApiComUseragentstringDataInterface
	 */
	public function setDescription(?string $description) : ApiComUseragentstringDataInterface
	{
		$this->_description = $description;
		
		return $this;
	}
	
	/**
	 * Gets the global description of the user agent.
	 * 
	 * @return ?string
	 */
	public function getDescription() : ?string
	{
		return $this->_description;
	}
	
	/**
	 * Sets the date when this user agent was seen for the first time.
	 * 
	 * @param ?DateTimeInterface $firstVisit
	 * @return ApiComUseragentstringDataInterface
	 */
	public function setFirstVisit(?DateTimeInterface $firstVisit) : ApiComUseragentstringDataInterface
	{
		$this->_firstVisit = $firstVisit;
		
		return $this;
	}
	
	/**
	 * Gets the date when this user agent was seen for the first time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getFirstVisit() : ?DateTimeInterface
	{
		return $this->_firstVisit;
	}
	
	/**
	 * Sets the date when this user agent was seen for the last time.
	 * 
	 * @param ?DateTimeInterface $lastVisit
	 * @return ApiComUseragentstringDataInterface
	 */
	public function setLastVisit(?DateTimeInterface $lastVisit) : ApiComUseragentstringDataInterface
	{
		$this->_lastVisit = $lastVisit;
		
		return $this;
	}
	
	/**
	 * Gets the date when this user agent was seen for the last time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getLastVisit() : ?DateTimeInterface
	{
		return $this->_lastVisit;
	}
	
}
