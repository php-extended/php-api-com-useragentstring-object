<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring\Test;

use DateTimeImmutable;
use PhpExtended\ApiComUseragentstring\ApiComUseragentstringData;
use PHPUnit\Framework\TestCase;

/**
 * ApiComUseragentstringDataTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUseragentstring\ApiComUseragentstringData
 * @internal
 * @small
 */
class ApiComUseragentstringDataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUseragentstringData
	 */
	protected ApiComUseragentstringData $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdentifier() : void
	{
		$this->assertEquals(12, $this->_object->getIdentifier());
		$expected = 25;
		$this->_object->setIdentifier($expected);
		$this->assertEquals($expected, $this->_object->getIdentifier());
	}
	
	public function testGetUserAgent() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getUserAgent());
		$expected = 'qsdfghjklm';
		$this->_object->setUserAgent($expected);
		$this->assertEquals($expected, $this->_object->getUserAgent());
	}
	
	public function testGetParts() : void
	{
		$this->assertEquals([], $this->_object->getParts());
		$expected = ['key1' => 'qsdfghjklm', 'key2' => 'qsdfghjklm'];
		$this->_object->setParts($expected);
		$this->assertEquals($expected, $this->_object->getParts());
	}
	
	public function testGetDescription() : void
	{
		$this->assertNull($this->_object->getDescription());
		$expected = 'qsdfghjklm';
		$this->_object->setDescription($expected);
		$this->assertEquals($expected, $this->_object->getDescription());
	}
	
	public function testGetFirstVisit() : void
	{
		$this->assertNull($this->_object->getFirstVisit());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setFirstVisit($expected);
		$this->assertEquals($expected, $this->_object->getFirstVisit());
	}
	
	public function testGetLastVisit() : void
	{
		$this->assertNull($this->_object->getLastVisit());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setLastVisit($expected);
		$this->assertEquals($expected, $this->_object->getLastVisit());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComUseragentstringData(12, 'azertyuiop');
	}
	
}
