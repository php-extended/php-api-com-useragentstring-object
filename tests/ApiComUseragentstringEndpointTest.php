<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiComUseragentstring\ApiComUseragentstringData;
use PhpExtended\ApiComUseragentstring\ApiComUseragentstringEndpoint;
use PhpExtended\ApiComUseragentstring\ApiComUseragentstringInfo;
use PhpExtended\ApiComUseragentstring\ApiComUseragentstringUserAgent;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiComUseragentstringEndpointTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUseragentstring\ApiComUseragentstringEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiComUseragentstringEndpointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUseragentstringEndpoint
	 */
	protected ApiComUseragentstringEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetUserAgent() : void
	{
		$this->assertInstanceOf(ApiComUseragentstringUserAgent::class, $this->_object->getUserAgent('Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'));
	}
	
	public function testGetUserAgentInfoIterator() : void
	{
		$k = 0;
		
		foreach($this->_object->getUserAgentInfoIterator() as $userAgent)
		{
			$this->assertInstanceOf(ApiComUseragentstringInfo::class, $userAgent);
			
			if(0 === $k)
			{
				$data = $this->_object->getUserAgentData($userAgent);
				$this->assertInstanceOf(ApiComUseragentstringData::class, $data);
			}
			
			$k++;
		}
		
		$this->assertGreaterThan(1, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0']];
				$data = \file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				$body = new StringStream($data);
				
				return (new Response())->withBody($body);
			}
		};
		
		$this->_object = new ApiComUseragentstringEndpoint($client);
	}
	
}
