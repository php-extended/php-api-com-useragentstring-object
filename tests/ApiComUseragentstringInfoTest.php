<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring\Test;

use PhpExtended\ApiComUseragentstring\ApiComUseragentstringInfo;
use PHPUnit\Framework\TestCase;

/**
 * ApiComUseragentstringInfoTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUseragentstring\ApiComUseragentstringInfo
 * @internal
 * @small
 */
class ApiComUseragentstringInfoTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUseragentstringInfo
	 */
	protected ApiComUseragentstringInfo $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFamily() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFamily());
		$expected = 'qsdfghjklm';
		$this->_object->setFamily($expected);
		$this->assertEquals($expected, $this->_object->getFamily());
	}
	
	public function testGetBrowser() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getBrowser());
		$expected = 'qsdfghjklm';
		$this->_object->setBrowser($expected);
		$this->assertEquals($expected, $this->_object->getBrowser());
	}
	
	public function testGetUserAgent() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getUserAgent());
		$expected = 'qsdfghjklm';
		$this->_object->setUserAgent($expected);
		$this->assertEquals($expected, $this->_object->getUserAgent());
	}
	
	public function testGetIdentifier() : void
	{
		$this->assertEquals(12, $this->_object->getIdentifier());
		$expected = 25;
		$this->_object->setIdentifier($expected);
		$this->assertEquals($expected, $this->_object->getIdentifier());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComUseragentstringInfo('azertyuiop', 'azertyuiop', 'azertyuiop', 12);
	}
	
}
