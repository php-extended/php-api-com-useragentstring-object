<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring\Test;

use PhpExtended\ApiComUseragentstring\ApiComUseragentstringUserAgent;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiComUseragentstringUserAgentTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComUseragentstring\ApiComUseragentstringUserAgent
 * @internal
 * @small
 */
class ApiComUseragentstringUserAgentTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComUseragentstringUserAgent
	 */
	protected ApiComUseragentstringUserAgent $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetAgentType() : void
	{
		$this->assertNull($this->_object->getAgentType());
		$expected = 'qsdfghjklm';
		$this->_object->setAgentType($expected);
		$this->assertEquals($expected, $this->_object->getAgentType());
	}
	
	public function testGetAgentName() : void
	{
		$this->assertNull($this->_object->getAgentName());
		$expected = 'qsdfghjklm';
		$this->_object->setAgentName($expected);
		$this->assertEquals($expected, $this->_object->getAgentName());
	}
	
	public function testGetAgentVersion() : void
	{
		$this->assertNull($this->_object->getAgentVersion());
		$expected = 'qsdfghjklm';
		$this->_object->setAgentVersion($expected);
		$this->assertEquals($expected, $this->_object->getAgentVersion());
	}
	
	public function testGetOsType() : void
	{
		$this->assertNull($this->_object->getOsType());
		$expected = 'qsdfghjklm';
		$this->_object->setOsType($expected);
		$this->assertEquals($expected, $this->_object->getOsType());
	}
	
	public function testGetOsName() : void
	{
		$this->assertNull($this->_object->getOsName());
		$expected = 'qsdfghjklm';
		$this->_object->setOsName($expected);
		$this->assertEquals($expected, $this->_object->getOsName());
	}
	
	public function testGetOsVersionName() : void
	{
		$this->assertNull($this->_object->getOsVersionName());
		$expected = 'qsdfghjklm';
		$this->_object->setOsVersionName($expected);
		$this->assertEquals($expected, $this->_object->getOsVersionName());
	}
	
	public function testGetOsVersionNumber() : void
	{
		$this->assertNull($this->_object->getOsVersionNumber());
		$expected = 'qsdfghjklm';
		$this->_object->setOsVersionNumber($expected);
		$this->assertEquals($expected, $this->_object->getOsVersionNumber());
	}
	
	public function testGetOsProducer() : void
	{
		$this->assertNull($this->_object->getOsProducer());
		$expected = 'qsdfghjklm';
		$this->_object->setOsProducer($expected);
		$this->assertEquals($expected, $this->_object->getOsProducer());
	}
	
	public function testGetOsProducerUrl() : void
	{
		$this->assertNull($this->_object->getOsProducerUrl());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setOsProducerUrl($expected);
		$this->assertEquals($expected, $this->_object->getOsProducerUrl());
	}
	
	public function testGetLinuxDistribution() : void
	{
		$this->assertNull($this->_object->getLinuxDistribution());
		$expected = 'qsdfghjklm';
		$this->_object->setLinuxDistribution($expected);
		$this->assertEquals($expected, $this->_object->getLinuxDistribution());
	}
	
	public function testGetAgentLanguage() : void
	{
		$this->assertNull($this->_object->getAgentLanguage());
		$expected = 'qsdfghjklm';
		$this->_object->setAgentLanguage($expected);
		$this->assertEquals($expected, $this->_object->getAgentLanguage());
	}
	
	public function testGetAgentLanguageTag() : void
	{
		$this->assertNull($this->_object->getAgentLanguageTag());
		$expected = 'qsdfghjklm';
		$this->_object->setAgentLanguageTag($expected);
		$this->assertEquals($expected, $this->_object->getAgentLanguageTag());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComUseragentstringUserAgent();
	}
	
}
